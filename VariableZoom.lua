local PERSISTENT_ZOOM = true	--Keep weapon zoom level between steelsight actions

if string.lower(RequiredScript) == "lib/units/beings/player/states/playerstandard" then

	local update_original = PlayerStandard.update
	local _end_action_steelsight_original = PlayerStandard._end_action_steelsight

	function PlayerStandard:update(t, dt, ...)
		update_original(self, t, dt, ...)
		self:_check_action_zoom(t, self:_get_extended_input(t, dt))
	end
	
	function PlayerStandard:_end_action_steelsight(...)
		if not PERSISTENT_ZOOM then
			self._equipped_unit:base():reset_zoom()
		end
		
		return _end_action_steelsight_original(self, ...)
	end

	function PlayerStandard:_get_extended_input(t, dt)
		local input = {}
		if self._controller:get_any_input_pressed() then
			input = {
				btn_zoom_in_press = self._controller:get_input_pressed("zoom_in"),
				btn_zoom_out_press = self._controller:get_input_pressed("zoom_out"),
			}
		end
		
		return input
	end
	
	function PlayerStandard:_check_action_zoom(t, input)
		if self._state_data.in_steelsight then
			local change
			
			if input.btn_zoom_in_press then
				change = self._equipped_unit:base():change_zoom(1)
			elseif input.btn_zoom_out_press then
				change = self._equipped_unit:base():change_zoom(-1)
			end
			
			if change then
				self:_stance_entered()
			end
		end
	end
	
elseif string.lower(RequiredScript) == "lib/units/weapons/newraycastweaponbase" then

	local _update_stats_values_original = NewRaycastWeaponBase._update_stats_values

	NewRaycastWeaponBase.DEFAULT_ZOOM_STEPS = 3
	
	NewRaycastWeaponBase.SCOPE_LEVELS = {
		wpn_fps_pis_c96_sight = { min = 1 },
		wpn_fps_upg_o_shortdot = { min = 1 },
		wpn_fps_upg_o_acog = { min = 1 },
		wpn_fps_upg_o_specter = { min = 1 },
		wpn_fps_upg_o_aimpoint = { min = 1 },
		wpn_fps_upg_o_aimpoint_2 = { min = 1 },
		wpn_fps_upg_o_rx01 = { min = 1 },
		wpn_fps_upg_o_rx30 = { min = 1 },
		wpn_fps_upg_winchester_o_classic = { min = 10, max = 12, zoom_steps = 3 },	--A5 scope
		wpn_fps_upg_o_leupold = { min = 10, max = 13, zoom_steps = 4 },	--Theia scope
	}
	
	NewRaycastWeaponBase.ZOOM_FOVS = {}
	for i, fov in ipairs(tweak_data.weapon.stats.zoom) do
		NewRaycastWeaponBase.ZOOM_FOVS[i] = fov
	end
	for i = 1 + #tweak_data.weapon.stats.zoom, 10 + #tweak_data.weapon.stats.zoom, 1 do
		NewRaycastWeaponBase.ZOOM_FOVS[i] = NewRaycastWeaponBase.ZOOM_FOVS[i-1] * 0.75
	end
	
	function NewRaycastWeaponBase:_update_stats_values(...)
		_update_stats_values_original(self, ...)

		if not self:is_npc() then
			local scope = {}
			local zoom_level = tweak_data.weapon[self._name_id].stats.zoom
			for part, data in pairs(self._parts) do
				local part_tweak = tweak_data.weapon.factory.parts[part]
				zoom_level = zoom_level + (part_tweak.stats and part_tweak.stats.zoom or 0)
				if NewRaycastWeaponBase.SCOPE_LEVELS[part] then
					scope = NewRaycastWeaponBase.SCOPE_LEVELS[part]
				end
			end
			local category = self:categories()[1] or ""
			zoom_level = zoom_level + managers.player:upgrade_value(category, "zoom_increase", 0)
			
			self._default_zoom_level = math.clamp(zoom_level, 1, #tweak_data.weapon.stats.zoom)
			self._min_zoom_level = (scope.min or 0) + (scope.min_delta or 0)
			self._max_zoom_level = (scope.max or self._default_zoom_level) + (scope.max_delta or 0)
			self._zoom_step_size = math.max((self._max_zoom_level - self._min_zoom_level) / (scope.zoom_steps or NewRaycastWeaponBase.DEFAULT_ZOOM_STEPS - 1), 1)
			
			self:set_zoom(self._current_zoom_level or self._default_zoom_level)
		end
	end

	function NewRaycastWeaponBase:reset_zoom()
		return self:set_zoom(self._default_zoom_level)
	end
	
	function NewRaycastWeaponBase:change_zoom(diff)
		return self:set_zoom(self._current_zoom_level + diff * self._zoom_step_size)
	end
	
	function NewRaycastWeaponBase:set_zoom(level)
		local old_zoom = self._current_zoom_level
		self._current_zoom_level = math.clamp(level, self._min_zoom_level, self._max_zoom_level)
		return self._current_zoom_level ~= old_zoom
	end
	
	--Overridden function
	function RaycastWeaponBase:zoom()
		local zoom = 60
	
		if self._current_zoom_level == 0 then
			local state = managers.player:player_unit():movement():current_state()
			local stances = tweak_data.player.stances.default
			local fov = state._state_data.ducking and stances.crouched.FOV or stances.standard.FOV or 65
			local mult = managers.user:get_setting("fov_multiplier")
			zoom = fov * (2*mult / (1+mult))
		else
			zoom = NewRaycastWeaponBase.ZOOM_FOVS[math.round(self._current_zoom_level)]
		end
		
		return zoom
	end

elseif string.lower(RequiredScript) == "lib/managers/controllerwrapper" then

	local init_original = ControllerWrapper.init

	function ControllerWrapper:init(manager, id, name, controller_map, default_controller_id, setup, ...)
		if setup._wrapper_type == "pc" and name == "player_1" then
			local zoom_in = deep_clone(setup._connection_map.primary_attack)
			local zoom_out = deep_clone(setup._connection_map.primary_attack)
			
			zoom_in._name = "zoom_in"
			zoom_in._controller_id = "mouse"
			zoom_in._input_name_list = { "mouse wheel up" }
			zoom_out._name = "zoom_out"
			zoom_out._controller_id = "mouse"
			zoom_out._input_name_list = { "mouse wheel down" }
			
			setup._connection_map.zoom_in = zoom_in
			setup._connection_map.zoom_out = zoom_out
			table.insert(setup._connection_list, "zoom_in")
			table.insert(setup._connection_list, "zoom_out")
		end
		
		return init_original(self, manager, id, name, controller_map, default_controller_id, setup, ...)
	end
	
end
