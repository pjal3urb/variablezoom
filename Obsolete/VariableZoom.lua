local NORMALIZED_SENSITIVITY = true	--Scale control sensitivity to current zoom level
local PERSISTENT_ZOOM = true	--Keep weapon zoom level between steelsight actions

if string.lower(RequiredScript) == "lib/units/beings/player/states/playerstandard" then

	local update_original = PlayerStandard.update
	local _end_action_steelsight_original = PlayerStandard._end_action_steelsight

	function PlayerStandard:update(t, dt, ...)
		update_original(self, t, dt, ...)
		self:_check_action_zoom(t, self:_get_extended_input(t, dt))
	end
	
	function PlayerStandard:_end_action_steelsight(...)
		if not PERSISTENT_ZOOM then
			self._equipped_unit:base():reset_zoom()
		end
		
		return _end_action_steelsight_original(self, ...)
	end

	function PlayerStandard:_get_extended_input(t, dt)
		local input = {}
		if self._controller:get_any_input_pressed() then
			input = {
				btn_zoom_in_press = self._controller:get_input_pressed("zoom_in"),
				btn_zoom_out_press = self._controller:get_input_pressed("zoom_out"),
			}
		end
		
		return input
	end
	
	function PlayerStandard:_check_action_zoom(t, input)
		if self._state_data.in_steelsight then
			local change
			
			if input.btn_zoom_in_press then
				change = self._equipped_unit:base():change_zoom(1)
			elseif input.btn_zoom_out_press then
				change = self._equipped_unit:base():change_zoom(-1)
			end
			
			if change then
				self:_stance_entered()
			end
		end
	end
	
	function PlayerStandard:get_fov_ratio()
		local stances = tweak_data.player.stances[self._equipped_unit:base():get_stance_id()] or tweak_data.player.stances.default
		if self:_is_meleeing() or self:_is_throwing_grenade() then
			stances = tweak_data.player.stances.default
		end
		
		local stance_data = self._state_data.in_steelsight and stances.steelsight or self._state_data.ducking and stances.crouched or stances.standard
		local standard_fov = (stance_data and stance_data.FOV or 75) * managers.user:get_setting("fov_multiplier")
		local current_fov = self:get_zoom_fov(stance_data)
		
		return current_fov / standard_fov
	end
	
elseif string.lower(RequiredScript) == "lib/units/weapons/newraycastweaponbase" then

	local _update_stats_values_original = NewRaycastWeaponBase._update_stats_values

	function NewRaycastWeaponBase:_update_stats_values(...)
		_update_stats_values_original(self, ...)

		local stats = tweak_data.weapon[self._name_id].stats
		local weapon_stats = tweak_data.weapon.stats
		local scope_level = 0
		local max_stat = #weapon_stats.zoom
		local base_stat = stats and stats.zoom or 2
		local upgrade_stat = managers.player:upgrade_value(self:weapon_tweak_data().category, "zoom_increase", 0)
		local mod_stat = 0
		
		local scopes = {
			wpn_fps_pis_c96_sight = 1,
			wpn_fps_upg_o_shortdot = 1,
			wpn_fps_upg_o_acog = 1,
			wpn_fps_upg_o_specter = 1,
			wpn_fps_upg_o_aimpoint = 1,
			wpn_fps_upg_o_aimpoint_2 = 1,
			wpn_fps_upg_o_rx01 = 1,
			wpn_fps_upg_o_rx30 = 1,
			wpn_fps_upg_winchester_o_classic = 2,	--A5 scope
			wpn_fps_upg_o_leupold = 2,	--Theia scope
		}
		
		for part, data in pairs(self._parts) do
			local part_tweak = tweak_data.weapon.factory.parts[part]
			local zoom_stat = part_tweak.stats and part_tweak.stats.zoom
			
			scope_level = math.max(scope_level, scopes[part] or 0)
			if zoom_stat then
				mod_stat = mod_stat + zoom_stat
			end
		end
		
		local zoom_levels = {}
		if scope_level == 0 then
			table.insert(zoom_levels, 0) --Default, non-ADS FOV special case
		end
		if scope_level <= 1 then
			table.insert(zoom_levels, weapon_stats.zoom[math.clamp(base_stat, 1, max_stat)])
			--table.insert(zoom_levels, weapon_stats.zoom[math.clamp(base_stat+upgrade_stat, 1, max_stat)])
		end
		table.insert(zoom_levels, weapon_stats.zoom[math.clamp(base_stat+mod_stat, 1, max_stat)])
		table.insert(zoom_levels, weapon_stats.zoom[math.clamp(base_stat+mod_stat+upgrade_stat, 1, max_stat)])
		table.sort(zoom_levels, function(x, y) return y ~= 0 and x > y end)
		
		self._zoom_levels = {}
		local tmp = {}
		for _, v in ipairs(zoom_levels) do
			if not tmp[v] then
				tmp[v] = true
				table.insert(self._zoom_levels, v)
			end
		end
		
		for i = 1, scope_level, 1 do
			table.insert(self._zoom_levels, self._zoom_levels[#self._zoom_levels] * 0.7)
		end
		
		self._default_zoom_level = #self._zoom_levels - scope_level
		self._current_zoom_level = self._default_zoom_level
	end

	function NewRaycastWeaponBase:reset_zoom()
		return self:set_zoom(self._default_zoom_level)
	end
	
	function NewRaycastWeaponBase:change_zoom(diff)
		return self:set_zoom(self._current_zoom_level + diff)
	end
	
	function NewRaycastWeaponBase:set_zoom(level)
		local old_zoom = self._current_zoom_level
		self._current_zoom_level = math.clamp(level, 1, #self._zoom_levels)
		return self._current_zoom_level ~= old_zoom
	end
	
	--Overridden function
	function RaycastWeaponBase:zoom()
		local zoom = self._zoom_levels[self._current_zoom_level]
		if zoom == 0 then
			local state = managers.player:player_unit():movement():current_state()
			local stances = tweak_data.player.stances.default
			local fov = state._state_data.ducking and stances.crouched.FOV or stances.standard.FOV or 65
			local mult = managers.user:get_setting("fov_multiplier")
			zoom = fov * (2*mult / (1+mult))
		end
		return zoom
	end

elseif string.lower(RequiredScript) == "lib/managers/controllerwrapper" then

	local init_original = ControllerWrapper.init

	function ControllerWrapper:init(manager, id, name, controller_map, default_controller_id, setup, ...)
		if setup._wrapper_type == "pc" and name == "player_1" then
			local zoom_in = deep_clone(setup._connection_map.primary_attack)
			local zoom_out = deep_clone(setup._connection_map.primary_attack)
			
			zoom_in._name = "zoom_in"
			zoom_in._controller_id = "mouse"
			zoom_in._input_name_list = { "mouse wheel up" }
			zoom_out._name = "zoom_out"
			zoom_out._controller_id = "mouse"
			zoom_out._input_name_list = { "mouse wheel down" }
			
			setup._connection_map.zoom_in = zoom_in
			setup._connection_map.zoom_out = zoom_out
			table.insert(setup._connection_list, "zoom_in")
			table.insert(setup._connection_list, "zoom_out")
		end
		
		return init_original(self, manager, id, name, controller_map, default_controller_id, setup, ...)
	end

elseif string.lower(RequiredScript) == "lib/managers/menumanager" and NORMALIZED_SENSITIVITY then

	--Override
	function MenuManager:set_mouse_sensitivity(zoomed)
		if self:is_console() then
			return
		end
		local sens = zoomed and managers.user:get_setting("enable_camera_zoom_sensitivity") and managers.user:get_setting("camera_zoom_sensitivity") or managers.user:get_setting("camera_sensitivity")
		local ratio = 1
		if managers.player and alive(managers.player:player_unit()) then
			ratio = managers.player:player_unit():movement():current_state():get_fov_ratio()
		end
		self._controller:get_setup():get_connection("look"):set_multiplier(sens * self._look_multiplier * ratio)
		managers.controller:rebind_connections()
	end
	
end
